<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 10/18/17
 * Time: 4:28 PM
 */

namespace DBSaver;


class FieldRules
{
    public static $fields = [
        "item_id"                           => ["required" => true, "max" => 1],
        "item_markets"                      => ["required" => true, "max" => 10],
        "item_ebay_id"                      => ["required" => false, "max" => 10],
        "item_asin"                         => ["required" => false, "max" => 10],
        "Item_no"                           => ["required" => true, "max" => 1],
        "item_mpn"                          => ["required" => true, "max" => 1],
        "item_other_part_no"                => ["required" => false, "max" => 1],
        "item_Interchange_no"               => ["required" => false, "max" => 1],
        "item_discontinued_no"              => ["required" => false, "max" => 1],
        "item_custom_no_name"               => ["required" => false, "max" => 1],
        "item_custom_no"                    => ["required" => false, "max" => 20],
        "item_epid"                         => ["required" => false, "max" => 1],
        "Item_upc"                          => ["required" => false, "max" => 10],
        "item_relevant"                     => ["required" => false, "max" => 1],
        "item_compare"                      => ["required" => false, "max" => 1],
        "item_additional"                   => ["required" => false, "max" => 1],
        "item_bundle"                       => ["required" => true, "max" => 1],
        "item_child"                        => ["required" => false, "max" => 10],
        "item_parent_id"                    => ["required" => false, "max" => 10],
        "item_supplier"                     => ["required" => false, "max" => 10],
        "item_location"                     => ["required" => false, "max" => 10],
        "item_quantity"                     => ["required" => false, "max" => 10],
        "item_location_quantity"            => ["required" => false, "max" => 10],
        "Item_quantity_limit"               => ["required" => true, "max" => 10],
        "item_shipping_services"            => ["required" => true, "max" => 10],
        "item_max_order_quantity"           => ["required" => true, "max" => 10],
        "item_handling_time"                => ["required" => false, "max" => 10],
        "item_map_price"                    => ["required" => false, "max" => 1],
        "item_purchase price_net"           => ["required" => false, "max" => 1],
        "item_transportation_costs_net"     => ["required" => false, "max" => 1],
        "item_storage_costs_net"            => ["required" => false, "max" => 1],
        "item_operating_costs"              => ["required" => false, "max" => 1],
        "item_shipping_cost"                => ["required" => false, "max" => 5],
        "item_extra_shipping_charge_"       => ["required" => false, "max" => 5],
        "item_price"                        => ["required" => true, "max" => 10],
        "item_min_price"                    => ["required" => true, "max" => 10],
        "item_max_price"                    => ["required" => true, "max" => 10],
        "item_sale_price"                   => ["required" => false, "max" => 10],
        "item_sale_start_date"              => ["required" => false, "max" => 10],
        "item_sale_end_date"                => ["required" => false, "max" => 10],
        "item_price_rule"                   => ["required" => false, "max" => 10],
        "item_price_starting_from_quantity" => ["required" => false, "max" => 10],
        "item_vat"                          => ["required" => false, "max" => 10],
        "Item_name"                         => ["required" => true, "max" => 10],
        "Item_subtitle"                     => ["required" => false, "max" => 10],
        "Item_short_description"            => ["required" => true, "max" => 10],
        "Item_description"                  => ["required" => true, "max" => 10],
        "Item_technical_detail"             => ["required" => true, "max" => 10],
        "Item_key_product_features"         => ["required" => false, "max" => 10],
        "item_notes"                        => ["required" => true, "max" => 10],
        "item_fitment_notes"                => ["required" => false, "max" => 10],
        "item_language"                     => ["required" => false, "max" => 10],
        "Item_origin-country"               => ["required" => true, "max" => 1],
        "Item_manufacturer"                 => ["required" => true, "max" => 1],
        "item_car_part"                     => ["required" => true, "max" => 1],
        "item_placement_on_vehicle"         => ["required" => true, "max" => 1],
        "item_part_type"                    => ["required" => true, "max" => 1],
        "item_category"                     => ["required" => true, "max" => 1],
        "item_series"                       => ["required" => true, "max" => 1],
        "item_exterior_finish"              => ["required" => true, "max" => 1],
        "item_material"                     => ["required" => true, "max" => 1],
        "Item_condition"                    => ["required" => true, "max" => 1],
        "item_condition_note"               => ["required" => false, "max" => 1],
        "item_warranty"                     => ["required" => true, "max" => 10],
        "item_custom_field_name"            => ["required" => false, "max" => 100],
        "item_custom_field"                 => ["required" => false, "max" => 100],
        "item_unit_of_measure"              => ["required" => false, "max" => 1],
        "Item_shipping_weight"              => ["required" => false, "max" => 1],
        "item_dimensions_whl"               => ["required" => false, "max" => 1],
        "item_width"                        => ["required" => false, "max" => 1],
        "item_height"                       => ["required" => false, "max" => 1],
        "item_length"                       => ["required" => false, "max" => 1],
        "item_weight"                       => ["required" => false, "max" => 1],
        "item_vehicle_compatible_text"      => ["required" => false, "max" => 1],
        "item_vehicle_compatible_make"      => ["required" => false, "max" => 1],
        "item_vehicle_compatible_model"     => ["required" => false, "max" => 1],
        "item_vehicle_compatible_years"     => ["required" => false, "max" => 1],
        "item_vehicle_compatible_type"      => ["required" => false, "max" => 1],
        "item_image"                        => ["required" => true, "max" => 10],
        "item_video"                        => ["required" => false, "max" => 10],
        "item_instructions_file"            => ["required" => false, "max" => 10],
        "item_file_source"                  => ["required" => false, "max" => 10],
        "item_html_template"                => ["required" => false, "max" => 10],
        "item_keywords"                     => ["required" => false, "max" => 10],
        "item_meta_title"                   => ["required" => true, "max" => 10],
        "item_meta_description"             => ["required" => true, "max" => 10],
        "Item_special"                      => ["required" => false, "max" => 10],
        "item_flag"                         => ["required" => false, "max" => 1],
        "item_flag_name"                    => ["required" => false, "max" => 1],
        "item_listings"                     => ["required" => false, "max" => 1],
        "listing_ld"                        => ["required" => false, "max" => 1],
        "listing_market"                    => ["required" => true, "max" => 1],
        "listing_no(sku)"                   => ["required" => true, "max" => 1],
        "listing_amazon_item-type"          => ["required" => false, "max" => 1],
        "listing_external_product_id_type"  => ["required" => true, "max" => 1],
        "listing_update_delete"             => ["required" => true, "max" => 1],
        "listing_duration"                  => ["required" => true, "max" => 1],
        "listing_duration_strategy"         => ["required" => false, "max" => 1],
        "listing_ebay_category"             => ["required" => true, "max" => 1],
        "listing_ebay_shop_category"        => ["required" => true, "max" => 1],
        "listing_specific_uses_keywords"    => ["required" => false, "max" => 10],
        "listing_ebay_specifics_name"       => ["required" => true, "max" => 1],
        "listing_ebay_specifics_value"      => ["required" => true, "max" => 1],
        "listing_payment_profile_name"      => ["required" => true, "max" => 1],
        "listing_shipping_profile_name"     => ["required" => true, "max" => 1],
        "listing_return_profile_name"       => ["required" => true, "max" => 1],
        "listing_flag"                      => ["required" => false, "max" => 1],
        "listing_flag_name"                 => ["required" => false, "max" => 1],
    ];

    public static function getRequiredFields()
    {
        $result = [];
        foreach (self::$fields as $key => $field) {
            if ($field["required"]) {
                $result[] = $key;
            }
        }

        return $result;
    }

}
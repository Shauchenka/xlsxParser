<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 10/18/17
 * Time: 4:19 PM
 */

namespace DBSaver;


class Field
{
    private $name;

    private $required = false;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Field
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return $this
     */
    public function setRequired()
    {
        $this->required = true;

        return $this;
    }
    /**
     * @return $this
     */
    public function setUnrequired()
    {
        $this->required = true;

        return $this;
    }

    /**
     * @return bool
     */
    public function isRequired()
    {
        return $this->required;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 10/18/17
 * Time: 6:09 PM
 */

namespace DBSaver;


class Logger
{
    const ERROR   = 0;
    const WARNING = 1;
    const SUCCESS = 2;

    private static $errorsCount = 0;

    private static $names = [
        self::ERROR   => "Error!",
        self::WARNING => "Warning!",
        self::SUCCESS => "Success!",
    ];

    /**
     * @param $message
     * @param $type
     */
    public static function store($message, $type)
    {
        self::$errorsCount++;
        file_put_contents(LOG_PATH, self::$names[$type] . " " . $message . PHP_EOL, FILE_APPEND);
    }

    /**
     * @return int
     *
     */
    public static function getErrorsCount()
    {
        return self::$errorsCount;
    }

    public static function clear()
    {
        file_put_contents(LOG_PATH, "");
    }

}
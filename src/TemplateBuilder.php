<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 10/18/17
 * Time: 4:17 PM
 */

namespace DBSaver;


class TemplateBuilder
{
    /**
     * @param $keys
     * @return array
     * @throws \Exception
     */
    public function getKeyObjects($keys)
    {
        $errors = [];
        $fieldList = [];
        foreach ($keys as $key) {
            $shortKey = $key;

            if(strpos($shortKey, "1-")){
                $shortKey = str_replace("1-", "", $shortKey);
            }
            $shortKey = preg_replace('/[0-9]+/', '', $shortKey);
            if (array_key_exists($shortKey, FieldRules::$fields)) {
                $rules = FieldRules::$fields[$shortKey];

                $field = new Field();

                if ($rules["required"]) {
                    $field->setRequired();
                }
                $name = $shortKey;

                if ($key !== $shortKey) { //if key without number
                    if(strpos("1-", $shortKey)){
                        $number = 1;
                    } else {
                        $number = filter_var($key, FILTER_SANITIZE_NUMBER_INT);
                        $name .= $number;
                    }

                    if (strlen($number) == 0) {
                        Logger::store("Column name [$key] not found not valid! ", Logger::ERROR);
                        $errors[] = "Column name [$key] not found not valid! ";

                    }

                    if ($number > $rules["max"]) {
                        Logger::store("Column name [$key] count mist be less then " . $rules["max"], Logger::ERROR);
                        $errors[] = "Column name [$key] count mist be less then " . $rules["max"];
                    }

                    if ($number != 1) {
                        $field->setUnrequired();
                    }
                }
                $field->setName($name);
                $fieldList[$name] = $field;
            } else {
                Logger::store("Column name [$key] not valid! ", Logger::ERROR);
                $errors[] = "Column name [$key] not valid! ";
            }
        }

        $requiredErrors = $this->validate($fieldList);

        $errors = array_merge($errors, $requiredErrors);

        return [
            "data"   => $fieldList,
            "errors" => $errors
        ];
    }

    /**
     * @param $fieldList
     * @return array
     * @throws \Exception
     */
    private function validate($fieldList)
    {
        $errors = [];

        $found = false;
        foreach (FieldRules::getRequiredFields() as $key => $requiredField) {
            /** @var Field $generatedField */
            foreach ($fieldList as $generatedField) {
                $shortKey = $generatedField->getName();
                if(strpos($shortKey, "1-")){
                    $shortKey = str_replace("1-", "", $shortKey);
                }
                $shortKey = preg_replace('/[0-9]+/', '', $shortKey);

                if ($requiredField == $shortKey) {
                    $found = true;
                    break;
                }
            }

            if (!$found) {
                Logger::store("Column with name [$requiredField] not found", Logger::ERROR);
                $errors[] = "Column with name [$requiredField] not found";

            }
        }

        return $errors;
    }
}
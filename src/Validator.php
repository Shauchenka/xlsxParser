<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 10/18/17
 * Time: 5:08 PM
 */

namespace DBSaver;


class Validator
{
    private $keyList;

    /**
     * Validator constructor.
     * @param $keyList
     */
    public function __construct($keyList)
    {
        $this->keyList = $keyList;
    }

    /**
     * @param $field
     * @param $value
     * @return bool
     */
    public function validate($field, $value)
    {
        /** @var Field $fieldObject */
        $fieldObject = $this->keyList[$field];

        if($fieldObject->isRequired() && (is_null($value) || strlen($value) == 0)){
            return false;
        }

        return true;
    }

}
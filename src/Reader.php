<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 10/18/17
 * Time: 6:19 PM
 */

namespace DBSaver;


use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;

class Reader
{
    /** @var  DBSaverInterface */
    private $dbSaver;

    /** @var  TemplateBuilder */
    private $templateBuilder;

    /**
     * Reader constructor.
     * @param DBSaverInterface $dbSaver
     */
    public function __construct(DBSaverInterface $dbSaver)
    {
        $this->dbSaver = $dbSaver;
        $this->templateBuilder = new TemplateBuilder();
    }

    /**
     * @param $filePath
     * @throws \Exception
     */
    public function checkFile($filePath)
    {

        if (!file_exists($filePath)) {
            throw new \Exception("File [$filePath] not found!");
        }

        $reader = ReaderFactory::create(Type::XLSX); // for XLSX files
        $reader->open($filePath);

        foreach ($reader->getSheetIterator() as $sheet) {
            foreach ($sheet->getRowIterator() as $row) {
                $newRow = [];
                foreach ($row as $one){
                    if(strlen($one) !== 0){
                        $newRow[] = str_replace('"', "", trim($one));
                    }
                }

                $row = $newRow;
                $result = $this->templateBuilder->getKeyObjects($row);
                if (count($result["errors"]) > 0) {
                    foreach ($result["errors"] as $error) {
                        echo $error . PHP_EOL;
                    }
                    throw new \Exception("File can't be parsed while errors are not eliminated!" . PHP_EOL);
                }
                break(2);
            }
        }
        echo "File is OK. Parsing started!" . PHP_EOL;
    }

    /**
     * @param $filePath
     */
    public function read($filePath)
    {
        $reader = ReaderFactory::create(Type::XLSX); // for XLSX files
        $reader->open($filePath);
        /** @var Validator $validator */
        $validator = null;
        $keys = [];
        $lineNumber = 1;
        $recordValid = true;

        foreach ($reader->getSheetIterator() as $sheet) {
            foreach ($sheet->getRowIterator() as $row) {
                if (is_null($validator)) {
                    $newRow = [];
                    foreach ($row as $one){
                        if(strlen($one) !== 0){
                            $newRow[] = str_replace('"', "", trim($one));
                        }
                    }

                    $row = $newRow;
                    $result = $this->templateBuilder->getKeyObjects($row);
                    $keyObjects = $result["data"];
                    $validator = new Validator($keyObjects);
                    $keys = array_keys($keyObjects);
                } else {
                    $row = array_slice($row, 0, count($keys));
                    if(empty($row[0])){
                        break(2);
                    }
                    $record = array_combine($keys, $row);
                    foreach ($record as $key => $value) {
                        if (!$validator->validate($key, $value)) {
                            Logger::store("Field validation error! Line number: [$lineNumber] Field: [$key]. Value: [$value]", Logger::ERROR);
                        }
                    }
                    if ($recordValid) {
                        $this->dbSaver->itemStorage($record);
                    }
                }

                $lineNumber++;
            }
        }

        $reader->close();
    }
}
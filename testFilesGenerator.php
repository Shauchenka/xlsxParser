<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 10/18/17
 * Time: 4:08 PM
 */

require_once 'vendor/autoload.php';

require_once 'src/DBSaverInterface.php';
require_once 'src/DBSaver.php';
require_once 'src/Reader.php';
require_once 'src/Logger.php';
require_once 'src/Field.php';
require_once 'src/FieldRules.php';
require_once 'src/TemplateBuilder.php';
require_once 'src/Validator.php';

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use DBSaver\FieldRules;

function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }

    return $randomString;
}

$writer = WriterFactory::create(Type::XLSX); // for XLSX files
$filePath = "test2222.xlsx";
$writer->openToFile($filePath); // write data to a file or to a PHP stream


$topFields = [];
foreach (FieldRules::$fields as $key => $field) {
    if ($field["required"] || rand(1, 10) <= 5) {
        if ($field["max"] > 1) {
            $count = rand(1, $field["max"]);
            for ($i = 1; $i < $count; $i++) {
                $topFields[] = $key . $i;
            }
        } else {
            $topFields[] = $key;
        }
    }
}
$writer->addRow($topFields); // add a row at a time

$row = [];
for ($i = 0; $i < 1000; $i++) {

    foreach ($topFields as $field) {
        $row[$field] = generateRandomString(rand(1, 20));
    }
    $writer->addRow($row);
}

$writer->close();



<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 10/18/17
 * Time: 4:08 PM
 */

require_once 'vendor/autoload.php';

require_once 'src/DBSaverInterface.php';
require_once 'src/DBSaver.php';
require_once 'src/Reader.php';
require_once 'src/Logger.php';
require_once 'src/Field.php';
require_once 'src/FieldRules.php';
require_once 'src/TemplateBuilder.php';
require_once 'src/Validator.php';



use DBSaver\Reader;
use DBSaver\DBSaver;
use DBSaver\Logger;

define("LOG_PATH", "logs.txt");


Logger::clear();
$reader = new Reader(new DBSaver());

$filePath = "min.xlsx";

try{
    $reader->checkFile($filePath);
    $reader->read($filePath);
    echo "Errors count: " . Logger::getErrorsCount();
} catch (Exception $e){
    echo $e->getMessage() . PHP_EOL;
}


